class Code 
  attr_reader :PEGS, :codelet
  
  def initialize(str = nil)
    @PEGS = [:red, :green, :blue, :yellow, :orange, :purple]
    if str == nil
      pegs = @PEGS.map.to_a
      pegs.shuffle!
      @codelet = pegs.take(4)
    else 
      @codelet = parse(str)
    end
  end
  
  def parse(str)
    str.scan(/[a-zA-Z]+/).map do |chars|
      chars.downcase.to_sym
    end
  end
  
  def is_valid?
    @codelet.count == 4 && @codelet.all? { |color| @PEGS.include?(color) }
  end
  
  def match(other_code)
    @codelet == other_code.codelet
  end
  
end

class Game
  
  def initialize
    @turns = 10
    @code = Code.new
    @guesses = []
    @exact_matches = []
  end
  
  def run
    won = false
    
    @turns.times do |i|
      guess = prompt
      @guesses << guess.codelet
      if guess.match @code
        won = true
        break
      else 
        check_guess(guess)
      end
    end
    
    if won
      puts "You won with #{ @guesses.count } tries"
      puts "Here are your guesses: \n"
      @guesses.each { |guess| puts guess.inspect }
    else
      puts "You lost, too many tries"
    end
    
    puts "Play again? (y/n)"
    answ = gets.chomp
    if answ[0].downcase == "y"
      g = Game.new
      g.run
    end
  end
  
  def prompt
    puts "Here are your guesses: \n"
    @guesses.each { |guess| puts guess.inspect }
    puts "\nHere are your exact matches: "
    p @exact_matches
    puts "\n\nEnter your guess (Try number #{@guesses.count + 1}): "
    pick = Code.new(gets.chomp)
    if pick.is_valid?
      return pick
    else 
      puts "Invalid guess, try again. Pick four colors"
      puts "The choices are: #{@code.PEGS}\n"
      prompt
    end
  end
  

  
  def check_guess(guess)
    inexact = []
    
    
    exact = guess.codelet.map.with_index do |peg, index|  
      if peg == @code.codelet[index]
        peg
      else
        inexact << peg if @code.codelet.include? peg
        nil
      end
    end
    
    temp_exacts = exact.zip @exact_matches
    @exact_matches = temp_exacts.map { |a, b| a || b }
    
    puts "\nYou guessed #{guess.codelet}"
    puts "Exact matches: #{exact}"
    puts "Right color, Wrong position: #{inexact.uniq}\n"
  end
  
end












