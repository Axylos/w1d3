class ComputerPlayer
  
  def reset
    @guessed_letters = []
    @word_blanks = []
  end
  
  def check(guess_letter)
    picks = @pick.split("").map.with_index do |letter, i| 
      if letter == guess_letter
        i
      end
    end.compact
    picks
  end
  
  def get_length
    @pick = get_words.sample
    @pick.length
  end
  
  def end_word
    puts "The word was #{@pick}"
  end
  
  def guess(word_blanks)
    @word_blanks = word_blanks
    @guessed_letters ||= []
    
    if @word_blanks.empty?
      guess_letter = max_char(correct_length_words.join)
    else
      guess_letter = compute_best_guess
    end
    @guessed_letters << guess_letter
    
    
    guess_letter
  end
  
  def compute_best_guess2
    correct_length_words = filter_length
    possible_words = filter_incorrect_words(correct_length_words)
    smarter_words = filter_position_letters(possible_words)
    max_char(smarter_words.join)
  end
  
  def compute_best_guess
    @length = Proc.new { |word| word.length == @word_blanks.count }
    words = get_words
    right_length_words = words.keep_if do |word| 
      filters.all? { |prc| prc.call(word) }
      
    end  
     p max_char(right_length_words.join) 
    
  end
  
  def filters
    f = [ 
      Proc.new do |word|
        @word_blanks.each_with_index do |char, idx| 
            next if char.nil?
            word[idx] == char  
          end
        end, 
          
      Proc.new { |word| word.length == @word_blanks.count },
    
      Proc.new do |word|
        @word_blanks.all? do |char|
         indices = get_indices(char)
         indices.all? { |idx| word[idx] == char || char.nil?}
        end
      end
    ]
  end      
  
  def filter_position_letters(words)
    words.keep_if { |word| blanks_fit(word) }
  end
  
  def blanks_fit(word)
    @word_blanks.all? do |char|
     indices = get_indices(char)
     indices.all? { |idx| word[idx] == char || char.nil?}
    end
  end
  
  def get_indices(char)
    indices = []
    
    @word_blanks.each_with_index do |el, i|
      indices << i if el == char
    end
    
    
    indices
  end
  
  def filter_length
    dict = get_words
    dict.keep_if { |word| word.length == @word_blanks.count }
  end
  
  def max_char(chars)
    unguessed_letters = ("a".."z").to_a.keep_if do |char| 
      !(@guessed_letters.include?(char))
    end
    
    unguessed_letters.max_by { |char| chars.count(char) }
  end
    
  def filter_incorrect_words(words)
      
      words.keep_if do |word|
        @word_blanks.each_with_index do |char, idx| 
        next if char.nil?
        
        word[idx] == char  
      end
    end
  end
    
  private
  
  def get_words
    File.readlines("dictionary.txt").map(&:chomp)
  end
  
end