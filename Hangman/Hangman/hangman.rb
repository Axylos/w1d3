require "./HumanPlayer.rb"
require "./ComputerPlayer.rb"
class Hangman
  
  TURNS = 15
  
  def initialize(picker, guesser)
    @picker = picker
    @guesser = guesser
  end
  
  def get_right_indices(guess_letter)
    right_indices = @picker.check(guess_letter)
    if !right_indices.empty?
      right_indices.each do |index|
        @word_blanks[index] = guess_letter
      end
    end
  end
  
  def is_valid?(guess)
    guess.length == 1 && !guess.scan(/[a-z]/).empty?
  end
  
  def win?
    @word_blanks.all?
  end
  
  def prompt
    letters = @word_blanks.map do |letter|
      letter.nil? ? "_" : letter
    end
    
    puts "#{letters.join}"
    puts "Guessed letters: #{@letters_guessed.inspect}\n"
    guess = @guesser.guess(@word_blanks).downcase
    if is_valid?(guess)
      return guess
    else
      puts "Invalid: guess only one letter"
      prompt
    end
  end
  
  def reset
    @picker.reset
    @guesser.reset
  end
    
  def run
    
    @word_blanks = Array.new(@picker.get_length) 
    @letters_guessed = Array.new
    TURNS.times do |i|
      guess = prompt
      get_right_indices(guess)
      @letters_guessed << guess
      if win?
        puts "Guessing player won with #{ i + 1 } guesses"
        break
      end

    end
    if !win?
      puts "Guessing player lost"
      @picker.end_word
    end
    
    reset
    
  end
  
end



