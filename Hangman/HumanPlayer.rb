class HumanPlayer
    
  def reset
  end
  
  def get_length
    puts "Enter word length"
    gets.chomp.to_i
  end
  
  def check(guess_letter)
    puts "Is \"#{guess_letter}\" in your word? (y/n)"
    answ = gets.chomp
    
    if answ[0].downcase == "y"
      puts "Please enter the matched positions of #{guess_letter}:"
      return gets.chomp.split(" ").map(&:to_i)
    else
      []
    end
  end
  
  def end_word
  end
  
  def guess(word_length)
    puts "What's your guess?"
    guess = gets.chomp
  end
end